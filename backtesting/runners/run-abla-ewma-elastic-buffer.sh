#!/bin/bash

# output file
outfile=$1

# dataset to process
infile=$2

# algo args
excessiveblocksize=$3
beta0=$4
n0=$5
gammaReciprocal=$6
zeta_xB7=$7
thetaReciprocal=$8
delta=$9

tmpsim=$(mktemp)

awk -F ',' '{OFS=",";print $3}' "$infile" \
| ../../implementation-c/bin/abla-ewma-elastic-buffer -excessiveblocksize $excessiveblocksize -ablaconfig $beta0,$n0,$gammaReciprocal,$zeta_xB7,$thetaReciprocal,$delta \
| awk -F ',' '{OFS=","; print $1,$2,$3,$4,$5,int($5*128/'$zeta_xB7') };' \
> $tmpsim

tmpact=$(mktemp)
tmpeb=$(mktemp)
tmpcf=$(mktemp)
tmpnb=$(mktemp)

awk -F ',' '{OFS=",";print $1,$3,$3}' "$infile" | ../../implementation-c/bin/aggregate -windowlength 144 | awk -F ',' '{OFS=",";print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10}' > $tmpact
awk -F ',' '{OFS=",";print $1,$2,$3}' $tmpsim | ../../implementation-c/bin/aggregate -windowlength 144 | awk -F ',' '{OFS=",";print $1,$11,$12,$13,$14}' > $tmpeb
awk -F ',' '{OFS=",";print $1,$2,$5}' $tmpsim | ../../implementation-c/bin/aggregate -windowlength 144 | awk -F ',' '{OFS=",";print $1,$11,$12}' > $tmpcf
awk -F ',' '{OFS=",";print $1,$2,$6}' $tmpsim | ../../implementation-c/bin/aggregate -windowlength 144 | awk -F ',' '{OFS=",";print $1,$11,$12}' > $tmpnb
sed -i '1 s/openExcessiveBlockSize/blocksizeLimit/' $tmpeb
sed -i '1 s/closeExcessiveBlockSize/blocksizeLimit/' $tmpeb
sed -i '1 s/openExcessiveBlockSize/controlBlockSize/' $tmpcf
sed -i '1 s/closeExcessiveBlockSize/controlBlockSize/' $tmpcf
sed -i '1 s/openExcessiveBlockSize/neutralBlockSize/' $tmpnb
sed -i '1 s/closeExcessiveBlockSize/neutralBlockSize/' $tmpnb
join --header  --nocheck-order -t, -1 1 -2 1 $tmpact $tmpeb >$tmpsim
join --header  --nocheck-order -t, -1 1 -2 1 $tmpsim $tmpcf >$tmpact
join --header  --nocheck-order -t, -1 1 -2 1 $tmpact $tmpnb >$outfile

rm $tmpact
rm $tmpsim
rm $tmpeb
rm $tmpcf
rm $tmpnb
