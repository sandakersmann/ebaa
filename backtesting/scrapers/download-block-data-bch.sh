block=$1
limit=$2
while [ $block -le $((limit-1000+1)) ]
do
    rows=$(curl -s 'https://demo.chaingraph.cash/v1/graphql' -X POST --data-binary '{"query":"query {\n  block(\n    limit: 1000\n    order_by: { height: asc }\n    where: {\n      height: { _gte: '$block' }\n      accepted_by: { node: { name: { _like: \"%mainnet\" } } }\n    }\n  ) {\n    height\n    timestamp\n    size_bytes\n  }\n}\n"}' | jq -r '.data.block | map([.height, .timestamp, .size_bytes] | join(",")) | join("\n")')
    readarray <<<"$rows" rows
    if [ ${#rows[@]} == 1000 ]
    then
        printf "%s" "${rows[@]}"
        block=$((block+1000))
    else
        echo $res > /dev/stderr
    fi
    sleep 1.1
done

