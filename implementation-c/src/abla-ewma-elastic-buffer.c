#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <inttypes.h>

typedef __uint128_t uint128_t;

// Utility functions

uint64_t muldiv(uint64_t x, uint64_t y, uint64_t z) {
    assert(z != 0);
    uint128_t res = ((uint128_t) x * y) / z;
    assert(res <= (uint128_t) UINT64_MAX);
    return (uint64_t) res;
}

uint64_t min(uint64_t x, uint64_t y) {
    return x < y ? x : y;
}

uint64_t max(uint64_t x, uint64_t y) {
    return x < y ? y : x;
}

// Constant 2^7, used as fixed precision for algorithm's "asymmetry
// factor" configuration value, e.g. we will store the real number 1.5
// as integer 192 so when we want to multiply or divide an integer with
// value of 1.5, we will do muldiv(value, 192, B7) or
// muldiv(value, B7, 192).
#define B7 (1ull<<7) // 2^7 = 128

// Algorithm configuration
typedef struct {
    // Initial control block size value, also used as floor value
    uint64_t epsilon0;
    // Initial elastic buffer size value, also used as floor value
    uint64_t beta0;
    // Last block height which will have the initial block size limit
    uint64_t n0;
    // Reciprocal of control function "forget factor" value
    uint64_t gammaReciprocal;
    // Control function "asymmetry factor" value
    uint64_t zeta_xB7;
    // Reciprocal of elastic buffer decay rate
    uint64_t thetaReciprocal;
    // Elastic buffer "gear factor"
    uint64_t delta;
} configABLA_t;

// Algorithm's internal state
// Note: limit for the block with blockHeight will be given by
// controlBlockSize + elasticBufferSize
typedef struct {
    // Block height for which the state applies
    uint64_t blockHeight;
    // Control function state
    uint64_t controlBlockSize;
    // Elastic buffer function state
    uint64_t elasticBufferSize;
} datumABLA_t;

#define MIN_ZETA_XB7 129 // zeta real value of 1.0078125
#define MAX_ZETA_XB7 256 // zeta real value of 2.0000000
#define MIN_GAMMA_RECIPROCAL 9484
#define MAX_GAMMA_RECIPROCAL 151744
#define MIN_DELTA 0
#define MAX_DELTA 32
#define MIN_THETA_RECIPROCAL 9484
#define MAX_THETA_RECIPROCAL 151744

// 18446744073709551615 / 256 * 128 = 9223372036854775680
#define MAX_SAFE_BLOCKSIZE_LIMIT (UINT64_MAX / MAX_ZETA_XB7 * B7)

// elastic_buffer_ratio_max = (delta * gamma / theta * (zeta - 1)) / (gamma / theta * (zeta - 1) + 1)
// 32 * (256 - 128) * 151744 / 9484 = 65536
#define MAX_ELASTIC_BUFFER_RATIO_NUMERATOR (MAX_DELTA * ((MAX_ZETA_XB7 - B7) * MAX_THETA_RECIPROCAL / MIN_GAMMA_RECIPROCAL))
// (256 - 128) * 151744 / 9484 + 128 = 2176
#define MAX_ELASTIC_BUFFER_RATIO_DENOMINATOR ((MAX_ZETA_XB7 - B7) * MAX_THETA_RECIPROCAL / MIN_GAMMA_RECIPROCAL + B7)

// 9223372036854775680 / (65536 + 2176) * 2176 = 296403260163573504
#define MAX_SAFE_CONTROL_BLOCKSIZE (MAX_SAFE_BLOCKSIZE_LIMIT / (MAX_ELASTIC_BUFFER_RATIO_NUMERATOR + MAX_ELASTIC_BUFFER_RATIO_DENOMINATOR) * MAX_ELASTIC_BUFFER_RATIO_DENOMINATOR)

// 9223372036854775680 - 296403260163573504 = 8926968776691202176
#define MAX_SAFE_ELASTIC_BUFFER_SIZE (MAX_SAFE_BLOCKSIZE_LIMIT - MAX_SAFE_CONTROL_BLOCKSIZE)

// Calculate the limit for the block to which the algorithm's state
// applies, given algorithm state, and algorithm configuration.
uint64_t getBlockSizeLimit(datumABLA_t *stateABLA)
{
    return (stateABLA->controlBlockSize + stateABLA->elasticBufferSize);
}

// Calculate algorithm's state for the next block (n), given 
// current blockchain tip (n-1) block size, algorithm state, and
// algorithm configuration.
datumABLA_t calculateNextABLAState (uint64_t currentBlockSize, datumABLA_t currentABLAState, configABLA_t *configABLA) {
    datumABLA_t nextABLAState;

    // n = n + 1
    nextABLAState.blockHeight = currentABLAState.blockHeight + 1;

    // use initialization values for block heights 0 to n0 inclusive
    if (nextABLAState.blockHeight <= configABLA->n0) {
        // epsilon_n = epsilon_0
        nextABLAState.controlBlockSize = configABLA->epsilon0;
        // beta_n = beta_0
        nextABLAState.elasticBufferSize = configABLA->beta0;
    }
    // algorithmic limit
    else {
        // control function

        // zeta * x_{n-1}
        uint64_t amplifiedCurrentBlockSize = muldiv(configABLA->zeta_xB7, currentBlockSize, B7);

        // if zeta * x_{n-1} > epsilon_{n-1} then increase
        if (amplifiedCurrentBlockSize > currentABLAState.controlBlockSize) {
            // zeta * x_{n-1} - epsilon_{n-1}
            uint64_t bytesToAdd = amplifiedCurrentBlockSize - currentABLAState.controlBlockSize;

            // zeta * y_{n-1}
            uint64_t amplifiedBlockSizeLimit = muldiv(configABLA->zeta_xB7, currentABLAState.controlBlockSize + currentABLAState.elasticBufferSize, B7);

            // zeta * y_{n-1} - epsilon_{n-1}
            uint64_t bytesMax = amplifiedBlockSizeLimit - currentABLAState.controlBlockSize;

            // zeta * beta_{n-1} * (zeta * x_{n-1} - epsilon_{n-1}) / (zeta * y_{n-1} - epsilon_{n-1})
            uint64_t scalingOffset = muldiv(muldiv(configABLA->zeta_xB7, currentABLAState.elasticBufferSize, B7), bytesToAdd, bytesMax);

            // epsilon_n = epsilon_{n-1} + gamma * (zeta * x_{n-1} - epsilon_{n-1} - zeta * beta_{n-1} * (zeta * x_{n-1} - epsilon_{n-1}) / (zeta * y_{n-1} - epsilon_{n-1}))
            nextABLAState.controlBlockSize = currentABLAState.controlBlockSize + (bytesToAdd - scalingOffset) / configABLA->gammaReciprocal;
        }
        // if zeta * x_{n-1} <= epsilon_{n-1} then decrease or no change
        else {
            // epsilon_{n-1} - zeta * x_{n-1}
            uint64_t bytesToRemove = currentABLAState.controlBlockSize - amplifiedCurrentBlockSize;

            // epsilon_{n-1} + gamma * (zeta * x_{n-1} - epsilon_{n-1})
            // rearranged to:
            // epsilon_{n-1} - gamma * (epsilon_{n-1} - zeta * x_{n-1})
            nextABLAState.controlBlockSize = currentABLAState.controlBlockSize - bytesToRemove / configABLA->gammaReciprocal;

            // epsilon_n = max(epsilon_{n-1} + gamma * (zeta * x_{n-1} - epsilon_{n-1}), epsilon_0)
            nextABLAState.controlBlockSize = max(nextABLAState.controlBlockSize, configABLA->epsilon0);
        }

        // elastic buffer function

        // beta_{n-1} * theta
        uint64_t bufferDecay = currentABLAState.elasticBufferSize / configABLA->thetaReciprocal;

        // if zeta * x_{n-1} > epsilon_{n-1} then increase
        if (amplifiedCurrentBlockSize > currentABLAState.controlBlockSize) {
            // (epsilon_{n} - epsilon_{n-1}) * delta
            uint64_t bytesToAdd = (nextABLAState.controlBlockSize - currentABLAState.controlBlockSize) * configABLA->delta;

            // beta_{n-1} - beta_{n-1} * theta + (epsilon_{n} - epsilon_{n-1}) * delta
            nextABLAState.elasticBufferSize = currentABLAState.elasticBufferSize - bufferDecay + bytesToAdd;
        }
        // if zeta * x_{n-1} <= epsilon_{n-1} then decrease or no change
        else {
            // beta_{n-1} - beta_{n-1} * theta
            nextABLAState.elasticBufferSize = currentABLAState.elasticBufferSize - bufferDecay;
        }
        // max(beta_{n-1} - beta_{n-1} * theta + (epsilon_{n} - epsilon_{n-1}) * delta, beta_0) , if zeta * x_{n-1} > epsilon_{n-1}
        // max(beta_{n-1} - beta_{n-1} * theta, beta_0) , if zeta * x_{n-1} <= epsilon_{n-1}
        nextABLAState.elasticBufferSize = max(nextABLAState.elasticBufferSize, configABLA->beta0);

        // clip controlBlockSize to MAX_SAFE_CONTROL_BLOCKSIZE to avoid integer overflow for extreme sizes
        nextABLAState.controlBlockSize = min(nextABLAState.controlBlockSize, MAX_SAFE_CONTROL_BLOCKSIZE);
        // clip elasticBufferSize to MAX_SAFE_ELASTIC_BUFFER_SIZE to avoid integer overflow for extreme sizes
        nextABLAState.elasticBufferSize = min(nextABLAState.elasticBufferSize, MAX_SAFE_ELASTIC_BUFFER_SIZE);
    }
    return nextABLAState;
}

#define ABLA_USAGE "Usage: ./abla <-excessiveblocksize 1000000> <-ablaconfig beta0,n0,gammaReciprocal,zeta,thetaReciprocal,delta [-ablacontinue blockHeight_n,elasticBufferSize_{n-1},controlBlockSize_{n-1}]>\n"
int main (int argc, char *argv[])
{
    uint64_t configInitialBlockSizeLimit;
    configABLA_t configABLA;
    datumABLA_t stateABLA;

    // Parse arguments
    if (argc < 3) {
        fprintf(stderr, ABLA_USAGE);
        return 1;
    }
    if (strcmp(argv[1], "-excessiveblocksize") == 0) {
        if (sscanf(argv[2], "%" PRIu64, &configInitialBlockSizeLimit) < 1) {
            fprintf(stderr, "Error, -excessiveblocksize argument missing.\n");
            return 1;
        }
    }
    else {
        fprintf(stderr, "Error, failed parsing -excessiveblocksize argument.\n");
        return 1;
    }
    if (argc > 4 && strcmp(argv[3], "-ablaconfig") == 0) {
        if (sscanf(argv[4], "%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64, &configABLA.beta0, &configABLA.n0, &configABLA.gammaReciprocal, &configABLA.zeta_xB7, &configABLA.thetaReciprocal, &configABLA.delta) < 6) {
            fprintf(stderr, "Error, failed parsing -ablaconfig arguments.\n");
            return 1;
        }

        // Finalize config
        if (configInitialBlockSizeLimit > configABLA.beta0) {
            configABLA.epsilon0 = configInitialBlockSizeLimit - configABLA.beta0;
        }
        else {
            fprintf(stderr, "Error, initial block size limit relative to initial elastic buffer size sanity check failed.\n");
            return 1;
        }

        // Sanity checks
        if (configABLA.epsilon0 + configABLA.beta0 > MAX_SAFE_BLOCKSIZE_LIMIT) {
            fprintf(stderr, "Error, initial block size limit sanity check failed (MAX_SAFE_BLOCKSIZE_LIMIT).\n");
            return 1;
        }
        if (configABLA.beta0 > MAX_SAFE_ELASTIC_BUFFER_SIZE) {
            fprintf(stderr, "Error, initial elastic buffer size sanity check failed (MAX_SAFE_ELASTIC_BUFFER_SIZE).\n");
            return 1;
        }
        if (configABLA.zeta_xB7 < MIN_ZETA_XB7 || configABLA.zeta_xB7 > MAX_ZETA_XB7) {
            fprintf(stderr, "Error, zeta sanity check failed.\n");
            return 1;
        }
        if (configABLA.gammaReciprocal < MIN_GAMMA_RECIPROCAL || configABLA.gammaReciprocal > MAX_GAMMA_RECIPROCAL) {
            fprintf(stderr, "Error, gammaReciprocal sanity check failed.\n");
            return 1;
        }
        if (configABLA.delta + 1 <= MIN_DELTA || configABLA.delta > MAX_DELTA) {
            fprintf(stderr, "Error, delta sanity check failed.\n");
            return 1;
        }
        if (configABLA.thetaReciprocal < MIN_THETA_RECIPROCAL || configABLA.thetaReciprocal > MAX_THETA_RECIPROCAL) {
            fprintf(stderr, "Error, thetaReciprocal sanity check failed.\n");
            return 1;
        }
        if (configABLA.epsilon0 < muldiv(configABLA.gammaReciprocal, B7, configABLA.zeta_xB7 - B7)) {
            // Required due to truncation of integer ops.
            // With this we ensure that the control size can be adjusted for at least 1 byte.
            // Also, with this we ensure that divisior bytesMax in calculateNextABLAState() can't be 0.
            fprintf(stderr, "Error, epsilon0 sanity check failed. Too low relative to gamma and zeta.\n");
            return 1;
        }

        // If we want to pick up from some height N instead of starting from height 0
        // then we must set the height & algorithm's state here.
        if (argc > 6 && strcmp(argv[5], "-ablacontinue") == 0) {
            if (sscanf(argv[6], "%" PRIu64 ",%" PRIu64 ",%" PRIu64, &stateABLA.blockHeight, &stateABLA.elasticBufferSize, &stateABLA.controlBlockSize) < 3) {
                fprintf(stderr, "Error, failed parsing -ablacontinue arguments.\n");
                return 1;
            }
            if (stateABLA.controlBlockSize < configABLA.epsilon0 || stateABLA.controlBlockSize > MAX_SAFE_BLOCKSIZE_LIMIT) {
                fprintf(stderr, "Error, invalid controlBlockSize state. Can't be neither below initialization value nor above MAX_SAFE_BLOCKSIZE_LIMIT.\n");
                return 1;
            }
            if (stateABLA.elasticBufferSize < configABLA.beta0 || stateABLA.elasticBufferSize > MAX_SAFE_ELASTIC_BUFFER_SIZE) {
                fprintf(stderr, "Error, invalid elasticBufferSize state. Can't be neither below initialization value nor above MAX_SAFE_ELASTIC_BUFFER_SIZE.\n");
                return 1;
            }
        }
        else {
            // Initialize state for height 0
            stateABLA.blockHeight = 0;
            stateABLA.controlBlockSize = configABLA.epsilon0;
            stateABLA.elasticBufferSize = configABLA.beta0;
        }
    }
    else {
        fprintf(stderr, "Error, -ablaconfig argument missing.\n");
        return 1;
    }

    // Calculate and print
    uint64_t blockSize;
    uint64_t blockSizeLimit;
    while (scanf("%" PRIu64, &blockSize) == 1) {
        // blockSize can't be greater than the limit, but if we're
        // testing against some dataset then we must clip it here.
        blockSize = min(blockSize, getBlockSizeLimit(&stateABLA));
        // calculate the next block's algorithm state
        stateABLA = calculateNextABLAState(blockSize, stateABLA, &configABLA);
        blockSizeLimit = getBlockSizeLimit(&stateABLA);
        printf("%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 "\n", stateABLA.blockHeight - 1, blockSize, blockSizeLimit, stateABLA.elasticBufferSize, stateABLA.controlBlockSize);
    }

    return 0;
}
