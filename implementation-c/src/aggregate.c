#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <math.h>

typedef __uint128_t uint128_t;

uint64_t min(uint64_t x, uint64_t y) {
    return x < y ? x : y;
}

uint64_t max(uint64_t x, uint64_t y) {
    return x < y ? y : x;
}

int getWindowData (uint64_t *windowBlockHeights, uint64_t *windowBlockSizes, uint64_t *windowExcessiveBlockSizes, size_t windowLength) {
    for (size_t i=0; i<windowLength; i++) {
        if (scanf("%lu,%lu,%lu\n", &windowBlockHeights[i], &windowBlockSizes[i], &windowExcessiveBlockSizes[i]) != 3)
            return 1;
    }
    return 0;
}

int cmpUint64 (const void * a, const void * b) {
    if (*(uint64_t*)a == *(uint64_t*)b)
        return 0;
    else
        return *(uint64_t*)a < *(uint64_t*)b ? -1 : 1;
}

void printWindowAggregates (uint64_t *windowBlockHeights, uint64_t *windowBlockSizes, uint64_t *windowExcessiveBlockSizes, size_t windowLength) {
    uint64_t minBlockSize;
    uint64_t *sortedBlockSizes = malloc(windowLength * sizeof(uint64_t));
    uint64_t medianBlockSize;
    uint64_t averageBlockSize = 0;
    uint128_t sumBlockSize = 0;
    uint64_t stdevBlockSize = 0;
    uint64_t maxBlockSize;
    uint64_t countExcessive = 0;
    uint64_t count90pExcessive = 0;
    uint64_t topDecile;
    uint64_t bottomDecile;
    assert(windowLength > 0);
    minBlockSize = windowBlockSizes[0];
    maxBlockSize = windowBlockSizes[0];

    memcpy(sortedBlockSizes, windowBlockSizes, windowLength * sizeof(uint64_t));
    qsort(sortedBlockSizes, windowLength, sizeof(uint64_t), cmpUint64);
    // median
    if (windowLength % 2 == 0)
        medianBlockSize = (sortedBlockSizes[windowLength / 2u - 1] +
                           sortedBlockSizes[windowLength / 2u]) / 2u;
    else
        medianBlockSize = sortedBlockSizes[windowLength / 2u];
    // bottom decile
    if (windowLength % 10 == 0)
        bottomDecile = (sortedBlockSizes[windowLength / 10u - 1] +
                           sortedBlockSizes[windowLength / 10u]) / 2u;
    else
        bottomDecile = sortedBlockSizes[windowLength / 10u];
    // top decile
    if (windowLength % 10 == 0)
        topDecile = (sortedBlockSizes[windowLength - windowLength / 10u] +
                           sortedBlockSizes[windowLength - windowLength / 10u - 1]) / 2u;
    else
        topDecile = sortedBlockSizes[windowLength - windowLength / 10u - 1];

    // min, max, average, counts
    for (size_t i=0; i<windowLength; i++) {
        minBlockSize = min(windowBlockSizes[i], minBlockSize);
        maxBlockSize = max(windowBlockSizes[i], maxBlockSize);
        sumBlockSize += windowBlockSizes[i];
        if (windowBlockSizes[i] > windowExcessiveBlockSizes[i])
            countExcessive++;
        if (windowBlockSizes[i] > windowExcessiveBlockSizes[i]*9u/10u)
            count90pExcessive++;
    }
    averageBlockSize = sumBlockSize / windowLength;

    // stdev
    long double stdev = 0;
    for (size_t i=0; i<windowLength; i++) {
        stdev += pow((long double) (averageBlockSize < windowBlockSizes[i]) ? (windowBlockSizes[i] - averageBlockSize) : (averageBlockSize - windowBlockSizes[i]), 2);
    }
    stdevBlockSize = (uint64_t) sqrtl(stdev / windowLength);

    printf("%lu,%lu,%lu,%lu,%lu,%lu,%lu,%lu,%lu,%lu,%lu,%lu,%lu,%lu,%lu%018lu\n", windowBlockHeights[0], windowBlockHeights[windowLength-1], minBlockSize, bottomDecile, averageBlockSize - min(stdevBlockSize, averageBlockSize), medianBlockSize, averageBlockSize, averageBlockSize + stdevBlockSize, topDecile, maxBlockSize, windowExcessiveBlockSizes[0], windowExcessiveBlockSizes[windowLength-1], countExcessive, count90pExcessive, (uint64_t) (sumBlockSize / (1000000000000000000)), (uint64_t) (sumBlockSize % (1000000000000000000)));

    free(sortedBlockSizes);
}

#define AGGREGATES_USAGE "Usage: ./aggregate -windowlength <144>\n"
int main (int argc, char *argv[])
{
    uint64_t windowLength = 0;

    // Process arguments
    if (argc < 3) {
        fprintf(stderr, AGGREGATES_USAGE);
        fprintf(stderr, "Input must be formatted as headerless .csv, with fields: blockHeight,blockSize,excessiveBlockSize\n");
        return 1;
    }
    if (!strcmp(argv[1], "-windowlength")) {
        if (sscanf(argv[2], "%lu", &windowLength) < 1) {
            fprintf(stderr, "Error, -windowlength argument missing.\n");
            return 1;
        }
    }
    else {
        fprintf(stderr, "Error, failed parsing -windowlength argument.\n");
        return 1;
    }

    // Initialize cache
    uint64_t *windowBlockHeights = malloc(windowLength * sizeof(uint64_t));
    uint64_t *windowBlockSizes = malloc(windowLength * sizeof(uint64_t));
    uint64_t *windowExcessiveBlockSizes = malloc(windowLength * sizeof(uint64_t));

    // Print row headers
    printf("openHeight,closeHeight,minBlockSize,bottomDecile,stdevBandLower,medianBlockSize,averageBlockSize,stdevBandUpper,topDecile,maxBlockSize,openExcessiveBlockSize,closeExcessiveBlockSize,countExcessive,count90pExcessive,sumBlockSize\n");

    // Calculate and print
    while (!getWindowData(windowBlockHeights, windowBlockSizes, windowExcessiveBlockSizes, windowLength))
        printWindowAggregates(windowBlockHeights, windowBlockSizes, windowExcessiveBlockSizes, windowLength);

    // Clean up
    free(windowBlockHeights);
    free(windowBlockSizes);
    free(windowExcessiveBlockSizes);
    return 0;
}
