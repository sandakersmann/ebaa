cmake_minimum_required(VERSION 3.5)

project(ebaa LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(EXECUTABLE_OUTPUT_PATH bin)

if (NOT MSVC)
    add_compile_options(-Wall -Wextra -pedantic -Werror)
endif()

add_executable(abla-ewma-elastic-buffer src/abla-ewma-elastic-buffer.cpp)

include(GNUInstallDirs)
install(TARGETS abla-ewma-elastic-buffer
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)

