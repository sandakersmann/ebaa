// abla-ewma-elastic-buffer
//
// Originally written by bitcoincashautist but converted to C++ by Calin Culianu.
// LICENSE: MIT
//
#include <algorithm>
#include <cassert>
#include <cinttypes>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <limits>
#include <string_view>

namespace ABLA {

// Utility function
static inline uint64_t muldiv(uint64_t x, uint64_t y, uint64_t z) {
    assert(z != 0);
    using uint128_t = __uint128_t ;
    const uint128_t res = (static_cast<uint128_t>(x) * static_cast<uint128_t>(y)) / static_cast<uint128_t>(z);
    assert(res <= static_cast<uint128_t>(std::numeric_limits<uint64_t>::max()));
    return static_cast<uint64_t>(res);
}

// Algorithm configuration
struct Config {
    // Initial control block size value, also used as floor value
    uint64_t epsilon0{};
    // Initial elastic buffer size value, also used as floor value
    uint64_t beta0{};
    // Last block height which will have the initial block size limit
    uint64_t n0{};
    // Reciprocal of control function "forget factor" value
    uint64_t gammaReciprocal{};
    // Control function "asymmetry factor" value
    uint64_t zeta_xB7{};
    // Reciprocal of elastic buffer decay rate
    uint64_t thetaReciprocal{};
    // Elastic buffer "gear factor"
    uint64_t delta{};

    // Constant 2^7, used as fixed precision for algorithm's "asymmetry
    // factor" configuration value, e.g. we will store the real number 1.5
    // as integer 192 so when we want to multiply or divide an integer with
    // value of 1.5, we will do muldiv(value, 192, B7) or
    // muldiv(value, B7, 192).
    static constexpr const uint64_t B7 = 1u << 7u;

    static constexpr const uint64_t MIN_ZETA_XB7 = 129u; // zeta real value of 1.0078125
    static constexpr const uint64_t MAX_ZETA_XB7 = 256u; // zeta real value of 2.0000000
    static constexpr const uint64_t MIN_GAMMA_RECIPROCAL = 9484u;
    static constexpr const uint64_t MAX_GAMMA_RECIPROCAL = 151744u;
    static constexpr const uint64_t MIN_DELTA = 0u;
    static constexpr const uint64_t MAX_DELTA = 32u;
    static constexpr const uint64_t MIN_THETA_RECIPROCAL = 9484u;
    static constexpr const uint64_t MAX_THETA_RECIPROCAL = 151744u;

    // 18446744073709551615 / 256 * 128 = 9223372036854775680
    static constexpr const uint64_t MAX_SAFE_BLOCKSIZE_LIMIT = std::numeric_limits<uint64_t>::max() / MAX_ZETA_XB7 * B7;

    // elastic_buffer_ratio_max = (delta * gamma / theta * (zeta - 1)) / (gamma / theta * (zeta - 1) + 1)
    // 32 * (256 - 128) * 151744 / 9484 = 65536
    static constexpr const uint64_t MAX_ELASTIC_BUFFER_RATIO_NUMERATOR = MAX_DELTA * ((MAX_ZETA_XB7 - B7) * MAX_THETA_RECIPROCAL / MIN_GAMMA_RECIPROCAL);
    // (256 - 128) * 151744 / 9484 + 128 = 2176
    static constexpr const uint64_t MAX_ELASTIC_BUFFER_RATIO_DENOMINATOR = (MAX_ZETA_XB7 - B7) * MAX_THETA_RECIPROCAL / MIN_GAMMA_RECIPROCAL + B7;

    // 9223372036854775680 / (65536 + 2176) * 2176 = 296403260163573504
    static constexpr const uint64_t MAX_SAFE_CONTROL_BLOCKSIZE = MAX_SAFE_BLOCKSIZE_LIMIT / (MAX_ELASTIC_BUFFER_RATIO_NUMERATOR + MAX_ELASTIC_BUFFER_RATIO_DENOMINATOR) * MAX_ELASTIC_BUFFER_RATIO_DENOMINATOR;

    // 9223372036854775680 - 296403260163573504 = 8926968776691202176
    static constexpr const uint64_t MAX_SAFE_ELASTIC_BUFFER_SIZE = MAX_SAFE_BLOCKSIZE_LIMIT - MAX_SAFE_CONTROL_BLOCKSIZE;

    // Returns true if the configuration is valid and/or sane. On false return optional out `*err` is set to point to
    // a constant string explaining the reason that it is invalid.
    [[nodiscard]] bool IsValid(const char **err = nullptr) const;
};

bool Config::IsValid(const char **err) const {
    if (epsilon0 + beta0 > MAX_SAFE_BLOCKSIZE_LIMIT) {
        if (err) *err = "Error, initial block size limit sanity check failed (MAX_SAFE_BLOCKSIZE_LIMIT)";
        return false;
    }
    if (beta0 > MAX_SAFE_ELASTIC_BUFFER_SIZE) {
        if (err) *err = "Error, initial elastic buffer size sanity check failed (MAX_SAFE_ELASTIC_BUFFER_SIZE).";
        return false;
    }
    if (zeta_xB7 < MIN_ZETA_XB7 || zeta_xB7 > MAX_ZETA_XB7) {
        if (err) *err = "Error, zeta sanity check failed.";
        return false;
    }
    if (gammaReciprocal < MIN_GAMMA_RECIPROCAL || gammaReciprocal > MAX_GAMMA_RECIPROCAL) {
        if (err) *err = "Error, gammaReciprocal sanity check failed.";
        return false;
    }
    if (delta + 1 <= MIN_DELTA || delta > MAX_DELTA) {
        if (err) *err = "Error, delta sanity check failed.";
        return false;
    }
    if (thetaReciprocal < MIN_THETA_RECIPROCAL || thetaReciprocal > MAX_THETA_RECIPROCAL) {
        if (err) *err = "Error, thetaReciprocal sanity check failed.";
        return false;
    }
    if (epsilon0 < muldiv(gammaReciprocal, B7, zeta_xB7 - B7)) {
        // Required due to truncation of integer ops.
        // With this we ensure that the control size can be adjusted for at least 1 byte.
        // Also, with this we ensure that divisior bytesMax in calculateNextABLAState() can't be 0.
        if (err) *err = "Error, epsilon0 sanity check failed. Too low relative to gamma and zeta.";
        return false;
    }
    if (err) *err = "";
    return true;
}

// Algorithm's internal state
// Note: limit for the block with blockHeight will be given by
// controlBlockSize + elasticBufferSize
struct State {
    // Block height for which the state applies
    uint64_t blockHeight{};
    // Control function state
    uint64_t controlBlockSize{};
    // Elastic buffer function state
    uint64_t elasticBufferSize{};

    // Calculate the limit for the block to which the algorithm's state
    // applies, given algorithm state
    uint64_t getBlockSizeLimit() const { return controlBlockSize + elasticBufferSize; }

    // Calculate algorithm's state for the next block (n), given
    // current blockchain tip (n-1) block size, algorithm state, and
    // algorithm configuration.
    State nextABLAState(const Config &config, uint64_t currentBlockSize) const;

    // Returns true if this state is valid relative to `config`. On false return, optional out `err` is set
    // to point to a constant string explaining the reason that this state is invalid.
    [[nodiscard]] bool IsValid(const Config &config, const char **err = nullptr) const;
};

// Calculate algorithm's state for the next block (n), given
// current blockchain tip (n-1) block size, algorithm state, and
// algorithm configuration. Returns the next state after this block.
State State::nextABLAState(const Config &config, const uint64_t currentBlockSize) const {
    // Next block DatumABLA
    State ret;

    // n = n + 1
    ret.blockHeight = this->blockHeight + 1ull;

    // use initialization values for block heights 0 to n0 inclusive
    if (ret.blockHeight <= config.n0) {
        // epsilon_n = epsilon_0
        ret.controlBlockSize = config.epsilon0;
        // beta_n = beta_0
        ret.elasticBufferSize = config.beta0;
    }
    // algorithmic limit
    else {
        // control function

        // zeta * x_{n-1}
        const uint64_t amplifiedCurrentBlockSize = muldiv(config.zeta_xB7, currentBlockSize, config.B7);

        // if zeta * x_{n-1} > epsilon_{n-1} then increase
        if (amplifiedCurrentBlockSize > this->controlBlockSize) {
            // zeta * x_{n-1} - epsilon_{n-1}
            const uint64_t bytesToAdd = amplifiedCurrentBlockSize - this->controlBlockSize;

            // zeta * y_{n-1}
            const uint64_t amplifiedBlockSizeLimit = muldiv(config.zeta_xB7, getBlockSizeLimit(), config.B7);

            // zeta * y_{n-1} - epsilon_{n-1}
            const uint64_t bytesMax = amplifiedBlockSizeLimit - this->controlBlockSize;

            // zeta * beta_{n-1} * (zeta * x_{n-1} - epsilon_{n-1}) / (zeta * y_{n-1} - epsilon_{n-1})
            const uint64_t scalingOffset = muldiv(muldiv(config.zeta_xB7, this->elasticBufferSize, config.B7),
                                                  bytesToAdd, bytesMax);

            // epsilon_n = epsilon_{n-1} + gamma * (zeta * x_{n-1} - epsilon_{n-1} - zeta * beta_{n-1} * (zeta * x_{n-1} - epsilon_{n-1}) / (zeta * y_{n-1} - epsilon_{n-1}))
            ret.controlBlockSize = this->controlBlockSize + (bytesToAdd - scalingOffset) / config.gammaReciprocal;
        }
        // if zeta * x_{n-1} <= epsilon_{n-1} then decrease or no change
        else {
            // epsilon_{n-1} - zeta * x_{n-1}
            const uint64_t bytesToRemove = this->controlBlockSize - amplifiedCurrentBlockSize;

            // epsilon_{n-1} + gamma * (zeta * x_{n-1} - epsilon_{n-1})
            // rearranged to:
            // epsilon_{n-1} - gamma * (epsilon_{n-1} - zeta * x_{n-1})
            ret.controlBlockSize = this->controlBlockSize - bytesToRemove / config.gammaReciprocal;

            // epsilon_n = max(epsilon_{n-1} + gamma * (zeta * x_{n-1} - epsilon_{n-1}), epsilon_0)
            ret.controlBlockSize = std::max(ret.controlBlockSize, config.epsilon0);
        }

        // elastic buffer function

        // beta_{n-1} * theta
        const uint64_t bufferDecay = this->elasticBufferSize / config.thetaReciprocal;

        // if zeta * x_{n-1} > epsilon_{n-1} then increase
        if (amplifiedCurrentBlockSize > this->controlBlockSize) {
            // (epsilon_{n} - epsilon_{n-1}) * delta
            const uint64_t bytesToAdd = (ret.controlBlockSize - this->controlBlockSize) * config.delta;

            // beta_{n-1} - beta_{n-1} * theta + (epsilon_{n} - epsilon_{n-1}) * delta
            ret.elasticBufferSize = this->elasticBufferSize - bufferDecay + bytesToAdd;
        }
        // if zeta * x_{n-1} <= epsilon_{n-1} then decrease or no change
        else {
            // beta_{n-1} - beta_{n-1} * theta
            ret.elasticBufferSize = this->elasticBufferSize - bufferDecay;
        }
        // max(beta_{n-1} - beta_{n-1} * theta + (epsilon_{n} - epsilon_{n-1}) * delta, beta_0) , if zeta * x_{n-1} > epsilon_{n-1}
        // max(beta_{n-1} - beta_{n-1} * theta, beta_0) , if zeta * x_{n-1} <= epsilon_{n-1}
        ret.elasticBufferSize = std::max(ret.elasticBufferSize, config.beta0);

        // clip controlBlockSize to MAX_SAFE_CONTROL_BLOCKSIZE to avoid integer overflow for extreme sizes
        ret.controlBlockSize = std::min(ret.controlBlockSize, config.MAX_SAFE_CONTROL_BLOCKSIZE);
        // clip elasticBufferSize to MAX_SAFE_ELASTIC_BUFFER_SIZE to avoid integer overflow for extreme sizes
        ret.elasticBufferSize = std::min(ret.elasticBufferSize, config.MAX_SAFE_ELASTIC_BUFFER_SIZE);
    }
    return ret;
}

bool State::IsValid(const Config &config, const char **err) const {
    if (controlBlockSize < config.epsilon0 || controlBlockSize > config.MAX_SAFE_BLOCKSIZE_LIMIT) {
        if (err) *err = "Error, invalid controlBlockSize state. Can't be neither below initialization value nor above MAX_SAFE_BLOCKSIZE_LIMIT.";
        return false;
    }
    if (elasticBufferSize < config.beta0 || elasticBufferSize > config.MAX_SAFE_ELASTIC_BUFFER_SIZE) {
        if (err) *err = "Error, invalid elasticBufferSize state. Can't be neither below initialization value nor above MAX_SAFE_ELASTIC_BUFFER_SIZE.";
        return false;
    }
    if (err) *err = "";
    return true;
}

} // namespace ABLA

static void printUsage(const char *progname)
{
    std::fprintf(stderr, "Usage: %s -excessiveblocksize 1000000"
                         " -ablaconfig beta0,n0,gammaReciprocal,zeta,thetaReciprocal,delta"
                         " [-ablacontinue blockHeight_n,elasticBufferSize_{n-1},controlBlockSize_{n-1}]\n",
                 progname);
}

int main(int argc, char *argv[])
{
    uint64_t configInitialBlockSizeLimit{};
    ABLA::Config config;
    ABLA::State state;
    using std::string_view_literals::operator""sv;

    // Parse arguments
    if (argc < 3) {
        printUsage(argv[0]);
        return EXIT_FAILURE;
    }
    if (argv[1] == "-excessiveblocksize"sv) {
        if (std::sscanf(argv[2], "%" PRIu64, &configInitialBlockSizeLimit) != 1) {
            std::fprintf(stderr, "Error, -excessiveblocksize argument missing.\n");
            return EXIT_FAILURE;
        }
    }
    else {
        std::fprintf(stderr, "Error, failed parsing -excessiveblocksize argument.\n");
        return EXIT_FAILURE;
    }
    if (argc > 4 && argv[3] == "-ablaconfig"sv) {
        if (std::sscanf(argv[4], "%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64,
                        &config.beta0, &config.n0, &config.gammaReciprocal, &config.zeta_xB7,
                        &config.thetaReciprocal, &config.delta) != 6) {
            std::fprintf(stderr, "Error, failed parsing -ablaconfig arguments.\n");
            return EXIT_FAILURE;
        }

        // Finalize config
        if (configInitialBlockSizeLimit > config.beta0) {
            config.epsilon0 = configInitialBlockSizeLimit - config.beta0;
        }
        else {
            std::fprintf(stderr, "Error, initial block size limit relative to initial elastic buffer size sanity check failed.\n");
            return EXIT_FAILURE;
        }

        // Sanity check the config
        const char *err{};
        if ( ! config.IsValid(&err)) {
            std::fprintf(stderr, "ABLA Config sanity check failed: %s\n", err);
            return EXIT_FAILURE;
        }

        // If we want to pick up from some height N instead of starting from height 0
        // then we must set the height & algorithm's state here.
        if (argc > 6 && argv[5] == "-ablacontinue"sv) {
            if (std::sscanf(argv[6], "%" PRIu64 ",%" PRIu64 ",%" PRIu64, &state.blockHeight, &state.elasticBufferSize, &state.controlBlockSize) != 3) {
                std::fprintf(stderr, "Error, failed parsing -ablacontinue arguments.\n");
                return EXIT_FAILURE;
            }

            // Sanity check the continued state object w.r.t. config
            if ( ! state.IsValid(config, &err)) {
                std::fprintf(stderr, "ABLA state sanity check failed: %s\n", err);
                return EXIT_FAILURE;
            }
        }
        else {
            // Initialize state for height 0
            state.blockHeight = 0;
            state.controlBlockSize = config.epsilon0;
            state.elasticBufferSize = config.beta0;
        }
    }
    else {
        std::fprintf(stderr, "Error, -ablaconfig argument missing.\n");
        return EXIT_FAILURE;
    }

    // Calculate and print
    uint64_t blockSize;
    while (std::scanf("%" PRIu64, &blockSize) == 1) {
        // blockSize can't be greater than the limit, but if we're
        // testing against some dataset then we must clip it here.
        blockSize = std::min(blockSize, state.getBlockSizeLimit());
        // calculate the next block's algorithm state
        state = state.nextABLAState(config, blockSize);
        const uint64_t blockSizeLimit = state.getBlockSizeLimit();
        std::printf("%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 "\n",
                    state.blockHeight - 1u, blockSize, blockSizeLimit, state.elasticBufferSize,
                    state.controlBlockSize);
    }

    return EXIT_SUCCESS;
}
