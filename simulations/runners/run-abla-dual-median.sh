#!/bin/bash

# output file
outfile=$1

# scenario test function to load
source $2

# algo args
excessiveblocksize=$3
n0=$4
alpha0_xB7_slow=$5
windowLength_slow=$6
alpha0_xB7_fast=$7
windowLength_fast=$8

tmpsim=$(mktemp)
tmpfifo=$(mktemp -u); mkfifo $tmpfifo

( echo 0,0,0,0,0; cat $tmpfifo ) | \
stdbuf -i0 -o0 awk -F ',' '{ print $3 };' | \
testfun | \
stdbuf -i0 -o0 ../../implementation-c/bin/abla-dual-median -excessiveblocksize $excessiveblocksize -ablaconfig $n0,$alpha0_xB7_slow,$windowLength_slow,$alpha0_xB7_fast,$windowLength_fast | \
tee $tmpsim >$tmpfifo

rm $tmpfifo

tmpeb=$(mktemp)
tmpcf=$(mktemp)
tmpnb=$(mktemp)


awk -F ',' '{OFS=",";print $1,$2,$3}' $tmpsim | ../../implementation-c/bin/aggregate -windowlength 144 > $tmpeb
awk -F ',' '{OFS=",";print $1,$2,(($4>$5)?$4:$5)}' $tmpsim | ../../implementation-c/bin/aggregate -windowlength 144 | awk -F ',' '{OFS=",";print $1,$11,$12}' > $tmpcf
sed -i '1 s/openExcessiveBlockSize/blockSizeLimit/' $tmpeb
sed -i '1 s/closeExcessiveBlockSize/blockSizeLimit/' $tmpeb
sed -i '1 s/openExcessiveBlockSize/dualMedianBlockSize/' $tmpcf
sed -i '1 s/closeExcessiveBlockSize/dualMedianBlockSize/' $tmpcf

join --header  --nocheck-order -t, -1 1 -2 1 $tmpeb $tmpcf >$outfile

rm $tmpsim
rm $tmpeb
rm $tmpcf
rm $tmpnb
