#!/bin/bash
bname="abla-ewma-elastic-buffer-bounded-01-ne-scenario-01"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../scenarios/scenario-01.sh" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 0 32000000 303514 128000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:256000000];" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-scenario-02"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../scenarios/scenario-02.sh" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:256000000];" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-scenario-03"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../scenarios/scenario-03.sh" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:256000000];" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-scenario-04"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../scenarios/scenario-04.sh" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:256000000];" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-scenario-05"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../scenarios/scenario-05.sh" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:256000000];" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-scenario-06"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../scenarios/scenario-06.sh" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:256000000];" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-scenario-07"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../scenarios/scenario-07.sh" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:256000000];" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-scenario-08"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../scenarios/scenario-08.sh" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:256000000];" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-scenario-09"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../scenarios/scenario-09.sh" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:256000000];" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-scenario-10"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../scenarios/scenario-10.sh" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:256000000];" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-scenario-11"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../scenarios/scenario-11.sh" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:256000000];" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-scenario-12"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../scenarios/scenario-12.sh" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:256000000];" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-scenario-13"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../scenarios/scenario-13.sh" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-scenario-14"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../scenarios/scenario-14.sh" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-scenario-15"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../scenarios/scenario-15.sh" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:256000000];" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-scenario-16"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../scenarios/scenario-16.sh" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:256000000];" plot-abla-ewma-elastic-buffer-bounded.gp
) &
