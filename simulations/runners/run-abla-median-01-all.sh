#!/bin/bash
bname="abla-median-01-scenario-01"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../scenarios/scenario-01.sh" 32000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-scenario-02"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../scenarios/scenario-02.sh" 32000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-scenario-03"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../scenarios/scenario-03.sh" 32000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-scenario-04"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../scenarios/scenario-04.sh" 32000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-scenario-05"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../scenarios/scenario-05.sh" 32000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-scenario-06"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../scenarios/scenario-06.sh" 32000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-scenario-07"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../scenarios/scenario-07.sh" 32000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-scenario-08"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../scenarios/scenario-08.sh" 32000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-scenario-09"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../scenarios/scenario-09.sh" 32000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-scenario-10"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../scenarios/scenario-09.sh" 32000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-scenario-11"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../scenarios/scenario-11.sh" 32000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-scenario-12"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../scenarios/scenario-12.sh" 32000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-scenario-13"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../scenarios/scenario-13.sh" 32000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-scenario-14"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../scenarios/scenario-14.sh" 32000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-scenario-15"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../scenarios/scenario-15.sh" 32000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-scenario-16"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../scenarios/scenario-16.sh" 32000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &
