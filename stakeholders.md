# Stakeholder Responses & Statements

## Statements

The following public statements have been submitted in response to this CHIP.

### Approve

The following statements have been submitted in support of this CHIP.

> The revisions to this CHIP have made it significantly easier to understand and clearly addresses all major concerns that I've seen raised in regards to this issue.
>
> As a Fulcrum server operator, I endorse this proposal. I know I'd be able to keep up with operation costs even if we implemented BIP-101 instead, which would presently have us at 64mb block capacity.
>
> I also recognize the urgency of solving this potential social attack before it becomes a problem again. I encourage adoption of this proposal for the November 2023 lock-in, allowing it to be activated in May 2024.
>
> —<cite>[kzKallisti](https://github.com/kzKallisti), [Selene](https://selene.cash/), [CashNinja](https://bch.ninja/)</cite>

> I am unable to comprehend the math, but I agree with and support the behaviour described and illustrated in various graphs. Further, I am impressed and content with the quality of research done in alternatives and simulating various edge cases and as such I am looking forward to activation of this improvement to managing our max blocksize limit.
>
> —<cite>[Jonathan Silverblood](https://gitlab.com/monsterbitar), Bitcoin Cash developer</cite>

>I (and The Bitcoin Cash Podcast & Selene Wallet) wholly endorse CHIP-2023-04 Adaptive Blocksize Limit Algorithm for Bitcoin Cash for lock-in 15 November 2023. A carefully selected algorithm that responds to real network demand is an obvious improvement to relieve social burden of discussion around optimal blocksizes plus implementation costs & uncertainty around scaling for miners & node operators. There is also some benefit to the community signalling its commitment to scaling & refusal to repeat the historic delays resulting from previous blocksize increase contention.
>
>The amount of work done by bitcoincashautist has been very impressive & inspiring. I refer to not only work on the spec itself but also on iteration from feedback & communicating with stakeholders to patiently address concerns across a variety of mediums. Having reviewed the CHIP thoroughly, I am convinced the chosen parameters accomodate edge cases in a technically sustainable manner.
>
>It is a matter of some urgency to lock in this CHIP for November. This will solidify the social contract to scale the BCH blocksize as demand justifies it, all the way to global reserve currency status. Furthermore, it will free up the community zeitgeist to tackle new problems for the 2025 upgrade.
>
>A blocksize algorithm implementation is a great step forward for the community. I look forward to this CHIP locking-in in November & going live in May 2024!
>
> —<cite>[Jeremy](https://bitcoincashpodcast.com/about), [The Bitcoin Cash Podcast](https://bitcoincashpodcast.com/), [Selene](https://selene.cash/)</cite>

### Disapprove

### Neutral
