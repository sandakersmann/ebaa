#!/bin/bash

mkdir -p json
mkdir -p csv
rm json/*
rm csv/*
rm *.csv

config1=(32000000 16000000 100000 37938 192 37938 10)
config2=(32000000 16000000 100000 9484 256 151744 32)

function test1() {
    awk -F ',' '{OFS=",";print $3}' ../backtesting/datasets/hypothetical-hybrid-x64.csv
}

function test2() {
    ./prng ace8de233d12e0359af31b718a4465a8c475dd71cdd0cd2b0496786f0e550a7c 420760 | sed 's/$/ % 32000000/g' | bc
    ./prng ace8de233d12e0359af31b718a4465a8c475dd71cdd0cd2b0496786f0e550a7d 420760 | sed 's/$/ % 128000000/g' | bc
    ./prng ace8de233d12e0359af31b718a4465a8c475dd71cdd0cd2b0496786f0e550a7e 420760 | sed 's/$/ % 512000000/g' | bc
    ./prng ace8de233d12e0359af31b718a4465a8c475dd71cdd0cd2b0496786f0e550a7f 420760 | sed 's/$/ % 2048000000/g' | bc
    ./prng ace8de233d12e0359af31b718a4465a8c475dd71cdd0cd2b0496786f0e550a80 420760 | sed 's/$/ % 8196000000/g' | bc
}

function test3() {
    ./prng ace8de233d12e0359af31b718a4465a8c475dd71cdd0cd2b0496786f0e550a7c 2103800
}

function test4() {
    for i in {1..3155700}; do
        echo 18446744073709551615
    done    
}

function test5() {
    for i in {1..2103800}; do
        echo 8196000000
    done
}

function test6() {
    for i in {1..210380}; do
        echo 18446744073709551615
    done
    for i in {1..420760}; do
        echo 0
    done
}

mkdir -p json

# set config

lconfig="config1"
config=("${config1[@]}")
sconfig="-excessiveblocksize ${config[0]} -ablaconfig ${config[1]},${config[2]},${config[3]},${config[4]},${config[5]},${config[6]}"
jsonconfig='{ "ABLAConfig": { "epsilon0": "'$(echo ${config[0]} - ${config[1]} | bc)'", "beta0": "'${config[1]}'", "n0": '${config[2]}', "zeta": '${config[4]}', "gammaReciprocal": '${config[3]}', "delta": '${config[6]}', "thetaReciprocal": '${config[5]}' }, '

# generate test vectors for the config

test="test1"
($test | ../implementation-c/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-c.csv") &
$test | ../implementation-cpp/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-cpp.csv"
wait

test="test2"
($test | ../implementation-c/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-c.csv") &
$test | ../implementation-cpp/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-cpp.csv"
wait

jsonf="activation-random32"
jsond="Test activation at configured height; using a random sampling in the 0 to 32 MB range; using proposed configuration with the exception of n0 which will be determined from MTP activation."
rowinit=99950
rowend=100050
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

jsonf="random32"
jsond="Test against a random sampling in the 0 to 32 MB range; using proposed configuration with the exception of n0 which will be determined from MTP activation."
rowinit=420000
rowend=420100
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

jsonf="random128"
jsond="Test against a random sampling in the 0 to 128 MB range; using proposed configuration with the exception of n0 which will be determined from MTP activation."
rowinit=840000
rowend=840100
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

jsonf="random512"
jsond="Test against a random sampling in the 0 to 512 MB range; using proposed configuration with the exception of n0 which will be determined from MTP activation."
rowinit=1260000
rowend=1260100
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

jsonf="random2048"
jsond="Test against a random sampling in the 0 to 2048 MB range; using proposed configuration with the exception of n0 which will be determined from MTP activation."
rowinit=1680000
rowend=1680100
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

jsonf="random8196"
jsond="Test against a random sampling in the 0 to 8196 MB range; using proposed configuration with the exception of n0 which will be determined from MTP activation."
rowinit=2100000
rowend=2100100
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

test="test3"
($test | ../implementation-c/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-c.csv") &
$test | ../implementation-cpp/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-cpp.csv"
wait

jsonf="hard-max-random"
jsond="Test with control blocksize at hard maximum; using a sequence of blocks of random size in uint64 range; using proposed configuration with the exception of n0 which will be determined from MTP activation."
rowinit=2101550
rowend=2101650
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

test="test4"
($test | ../implementation-c/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-c.csv") &
$test | ../implementation-cpp/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-cpp.csv"
wait

jsonf="hard-max-control-blocksize"
jsond="Test with (elastic buffer size / control blocksize) ratio at maximum stretch and reaching control blocksize hard maximum; using a sequence of blocks mined at maximum size; using proposed configuration with the exception of n0 which will be determined from MTP activation."
rowinit=1894050
rowend=1894150
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

jsonf="hard-max-elastic-buffer"
jsond="Test maxing out the elastic buffer when control blocksize is already at hard maximum; using a sequence of blocks mined at maximum size; using proposed configuration with the exception of n0 which will be determined from MTP activation."
rowinit=3061550
rowend=3061650
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

test="test5"
($test | ../implementation-c/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-c.csv") &
$test | ../implementation-cpp/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-cpp.csv"
wait

jsonf="elastic-buffer-max"
jsond="Test elastic buffer maxing out and decay taking over; using a sequence of blocks mined at flat size; using proposed configuration with the exception of n0 which will be determined from MTP activation."
rowinit=625050
rowend=625150
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

jsonf="blocksize-limit-max"
jsond="Test blocksize limit maxing out and then reducing due to elastic buffer decay taking over; using a sequence of blocks mined at flat size; using proposed configuration with the exception of n0 which will be determined from MTP activation."
rowinit=745250
rowend=745350
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

jsonf="elastic-buffer-floor"
jsond="Test elastic buffer decaying down to floor value; using a sequence of blocks mined at flat size; using proposed configuration with the exception of n0 which will be determined from MTP activation."
rowinit=1276500
rowend=1276600
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

test="test6"
($test | ../implementation-c/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-c.csv") &
$test | ../implementation-cpp/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-cpp.csv"
wait

jsonf="control-blocksize-floor-empty"
jsond="Test control blocksize reducing down to floor value; using a sequence of empty blocks; using proposed configuration with the exception of n0 which will be determined from MTP activation."
rowinit=265450
rowend=265550
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

jsonf="elastic-buffer-floor-empty"
jsond="Test elastic buffer decaying down to floor value; using a sequence of empty blocks; using proposed configuration with the exception of n0 which will be determined from MTP activation."
rowinit=310800
rowend=310900
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

# set config

lconfig="config2"
config=("${config2[@]}")
sconfig="-excessiveblocksize ${config[0]} -ablaconfig ${config[1]},${config[2]},${config[3]},${config[4]},${config[5]},${config[6]}"
jsonconfig='{ "ABLAConfig": { "epsilon0": "'$(echo ${config[0]} - ${config[1]} | bc)'", "beta0": "'${config[1]}'", "n0": '${config[2]}', "zeta": '${config[4]}', "gammaReciprocal": '${config[3]}', "delta": '${config[6]}', "thetaReciprocal": '${config[5]}' }, '

# generate test vectors for the config

test="test1"
($test | ../implementation-c/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-c.csv") &
$test | ../implementation-cpp/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-cpp.csv"
wait

test="test2"
($test | ../implementation-c/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-c.csv") &
$test | ../implementation-cpp/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-cpp.csv"
wait

jsonf="activation-random32"
jsond="Test activation at configured height; using a random sampling in the 0 to 32 MB range; Using alternative config at the extreme of sanity range of parameters."
rowinit=99950
rowend=100050
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

jsonf="random32"
jsond="Test against a random sampling in the 0 to 32 MB range; Using alternative config at the extreme of sanity range of parameters."
rowinit=420000
rowend=420100
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

jsonf="random128"
jsond="Test against a random sampling in the 0 to 128 MB range; Using alternative config at the extreme of sanity range of parameters."
rowinit=840000
rowend=840100
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

jsonf="random512"
jsond="Test against a random sampling in the 0 to 512 MB range; Using alternative config at the extreme of sanity range of parameters."
rowinit=1260000
rowend=1260100
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

jsonf="random2048"
jsond="Test against a random sampling in the 0 to 2048 MB range; Using alternative config at the extreme of sanity range of parameters."
rowinit=1680000
rowend=1680100
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

jsonf="random8196"
jsond="Test against a random sampling in the 0 to 8196 MB range; Using alternative config at the extreme of sanity range of parameters."
rowinit=2100000
rowend=2100100
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

test="test3"
($test | ../implementation-c/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-c.csv") &
$test | ../implementation-cpp/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-cpp.csv"
wait

jsonf="hard-max-random"
jsond="Test with control blocksize at hard maximum; using a sequence of blocks of random size in uint64 range; Using alternative config at the extreme of sanity range of parameters."
rowinit=2101550
rowend=2101650
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

test="test4"
($test | ../implementation-c/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-c.csv") &
$test | ../implementation-cpp/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-cpp.csv"
wait

jsonf="hard-max"
jsond="Test with (elastic buffer size / control blocksize) ratio at maximum stretch and both parameters reaching hard maximum; Using alternative config at the extreme of sanity range of parameters."
rowinit=324200
rowend=324300
jsondesc=' "testDescription": "'"$jsond"'", '
jsoninita=($(awk -F',' 'FNR =='$rowinit' {print $1,$3,$4,$5}' "$lconfig-$test-c.csv"))
jsoninit=' "ABLAStateInitial": { "n": '$((${jsoninita[0]}+1))', "epsilon": "'${jsoninita[3]}'", "beta": "'${jsoninita[2]}'" }, "blocksizeLimitInitial": "'${jsoninita[1]}'", '
jsonvec=' "testVector": [ '$(awk -F',' 'FNR >'$rowinit' && FNR < '$rowend' {print " { \"blocksize\": \""$2"\""," \"ABLAStateForNextBlock\": { \"n\": "$1+1," \"epsilon\": \""$5"\""," \"beta\": \""$4"\" }"," \"blocksizeLimitForNextBlock\": \""$3"\" }"} {OFS=","} {ORS=","}' "$lconfig-$test-c.csv" | sed 's/,$//')' ]'
echo $jsonconfig $jsondesc $jsoninit $jsonvec '}' | jq > json/"$lconfig-$test-$jsonf".json
awk -F',' 'FNR >='$rowinit' && FNR <'$rowend' {print $0} {OFS=","}' "$lconfig-$test-c.csv" > csv/"$lconfig-$test-$jsonf".csv

test="test5"
($test | ../implementation-c/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-c.csv") &
$test | ../implementation-cpp/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-cpp.csv"
wait

test="test6"
($test | ../implementation-c/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-c.csv") &
$test | ../implementation-cpp/bin/abla-ewma-elastic-buffer $sconfig >"$lconfig-$test-cpp.csv"
wait
